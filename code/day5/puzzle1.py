# load libraries
import pandas as pd
import re

# load crate data
crates = pd.read_fwf("input_data/input_day5.txt", widths=[4,4,4,4,4,4,4,4,4], nrows=8,
    header=None, names=["1", "2", "3", "4", "5", "6", "7", "8", "9"])

# convert to dict of lists for easier manipulation, removing empty values (with stack.())
crates_dict = crates.stack().groupby(level=1).apply(list).to_dict()

# load move data
with open("input_data/input_day5.txt", "r") as infile:
    moves = infile.readlines()[10:]

# move crates!
for line in moves:
    line = line.rstrip()
    [m, f, t] = [line.split(" ")[i] for i in (1,3,5)]
    for i in range(1,int(m)+1):
        crates_dict[t].insert(0, crates_dict[f][0])
        crates_dict[f].pop(0)

# print result
final = ""
for stack in sorted(crates_dict):
    top = crates_dict[stack][0]
    tmp = re.search(r"\[([A-Z])]", top)
    clean = tmp.group(1)
    final += clean

print(final)