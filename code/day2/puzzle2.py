# create score, outcome, and strategy dictionaries
scores = {
    "A": 1,
    "B": 2,
    "C": 3,
    "loss": 0,
    "draw": 3,
    "win": 6
}

outcomes = {
    "win": [("A", "B"), ("B", "C"), ("C", "A")],
    "draw": [("A", "A"), ("B", "B"), ("C", "C")],
    "loss": [("A", "C"), ("B", "A"), ("C", "B")]
}

strategy = {
    "X": "loss",
    "Y": "draw",
    "Z": "win"
}

# load and analyse input for the second day
with open("input_data/input_day2.txt", "r") as infile:
    total = 0
    for line in infile:
        line = line.rstrip()
        [o, m] = line.split(" ")
        outcome = strategy[m]
        total += scores[outcome]
        s = list(zip(*outcomes[outcome]))[0] #opponent's shape
        i = s.index(o) # tuple index
        c = outcomes[outcome][i][1] # shape I need to make
        total += scores[c]
        
print("total: {}".format(total))
