# create score and outcome dictionaries
scores = {
    "X": 1,
    "Y": 2,
    "Z": 3,
    "loss": 0,
    "draw": 3,
    "win": 6
}

outcomes = {
    "win": [("A", "Y"), ("B", "Z"), ("C", "X")],
    "draw": [("A", "X"), ("B", "Y"), ("C", "Z")],
    "loss": [("A", "Z"), ("B", "X"), ("C", "Y")]
}

# load and analyse input for the second day
with open("input_data/input_day2.txt", "r") as infile:
    outcome = ""
    total = 0
    for line in infile:
        line = line.rstrip()
        [o, m] = line.split(" ")
        if (o, m) in outcomes["win"]:
            outcome = "win"
        elif (o, m) in outcomes["draw"]:
            outcome = "draw"
        else:
            outcome = "loss"
        score = scores[m] + scores[outcome]
        total += score
 
print("total: {}".format(total))