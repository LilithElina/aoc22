# load and analyse input for the fourth day
with open("input_data/input_day4.txt", "r") as infile:
    total = 0
    for line in infile:
        line = line.rstrip()
        (r1, r2) = line.split(",")
        (s1, e1) = r1.split("-")
        (s2, e2) = r2.split("-")
        if int(s1) <= int(e2) and int(s2) <= int(e1):
            print(line)
            total += 1

print("total: {}".format(total))