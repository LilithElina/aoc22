# load and analyse input for the fourth day
with open("input_data/input_day4.txt", "r") as infile:
    total = 0
    for line in infile:
        line = line.rstrip()
        (r1, r2) = line.split(",")
        (s1, e1) = r1.split("-")
        (s2, e2) = r2.split("-")
        sec1 = range(int(s1), int(e1)+1)
        sec2 = range(int(s2), int(e2)+1)
        if int(s2) in sec1 and int(e2) in sec1:
            total += 1
        elif int(s1) in sec2 and int(e1) in sec2:
            total += 1

print("total: {}".format(total))