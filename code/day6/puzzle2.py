# read input data
with open("input_data/input_day6.txt", "r") as infile:
    f = infile.readlines()
    string = f[0]

# set start of marker
s = 0

# loop over blocks of four letters and look for duplicates
for i in range(14,len(string)+1):
    sub = string[s:i]
    duplicates = [l for l in sub if sub.count(l) > 1]
    if not duplicates:
        break
    else:
        s += 1
print(sub)
print(i)