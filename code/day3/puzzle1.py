# import library
import string

# prepare item list
items = list(string.ascii_letters)

# load and analyse input for the third day
with open("input_data/input_day3.txt", "r") as infile:
    total = 0
    for line in infile:
        line = line.rstrip()
        l = len(line)
        c1 = line[0:l//2]
        c2 = line[l//2:]
        for i in c1:
            if i in c2:
                prio = items.index(i) + 1
                total += prio
                break

print("total: {}".format(total))