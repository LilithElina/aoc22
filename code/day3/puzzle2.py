# import library
import string

# prepare item list
items = list(string.ascii_letters)

# load and analyse input for the third day
with open("input_data/input_day3.txt", "r") as infile:
    total = 0
    count = 1
    group = []
    for line in infile:
        line = line.rstrip()
        if count == 3:
            group.append(line)
            item = set.intersection(*map(set, group))
            i = list(item)[0]
            prio = items.index(i) + 1
            total += prio
            count = 1
            group = []
        else:
            count += 1
            group.append(line)

print("total: {}".format(total))