# import defaultdict
from collections import defaultdict

# create directory dictionaries
dirs = defaultdict(int)

# create other variables
path = [""]
dir = "/"
sum = 0

# read the input and collect directory sizes
with open("input_data/input_day7.txt", "r") as infile:
    for line in infile:
        line = line.rstrip()
        # get lines with commands
        if line.startswith("$"):
            com = line.split(" ")
            if com[1] == "cd":
                # go up the tree again using the path list
                if com[2] == "..":
                    prev_dir = dir
                    path.pop()
                    dir = path[-1]
                    #print("UP\tdir: {}, prev_dir: {}".format(dir, prev_dir))
                    dirs[dir] += dirs[prev_dir]
                # go down the tree and add directory to path list
                else:
                    dir = com[2]
                    #print("DOWN\tdir: {}, prev_dir: {}".format(dir, path[-1]))
                    path.append(dir)
        # skip line listing subdirectory
        elif line.startswith("dir"):
            pass
        # add file size to current directory
        else:
            size = line.split(" ")[0]
            dirs[dir] += int(size)

print(dirs["/"])

# re-calculate size of root
dirs["/"] = 0

with open("input_data/input_day7.txt", "r") as infile:
    next(infile)
    for line in infile:
        line = line.rstrip()
        # skip ls command
        if line.startswith("$ ls"):
            pass
        # add already calculated sized for directories
        elif line.startswith("dir"):
            dir = line.split(" ")[1]
            dirs["/"] += dirs[dir]
        # don't go down the tree again
        elif line.startswith("$ cd"):
            break
        # add file sizes
        else:
            size = line.split(" ")[0]
            dirs["/"] += int(size)

print(dirs)
print(path)

# find directories with size <= 100000
for s in dirs.values():
    if s <= 100000:
        sum += s

print(sum)