# initialise empty dictionary
sums = {}

# load input for the first day
with open("input_data/input_day1.txt", "r") as infile:
    calories = []
    elf = 1
    for line in infile:
        if line.strip():
            cal = int(line)
            calories.append(cal)
        else:
            sums[elf] = sum(calories)
            calories = []
            elf += 1
    sums[elf] = sum(calories)

# find the three elves with the most calories
cals = list(sums.values())
cals.sort(reverse=True)
print(cals[0]+cals[1]+cals[2])
