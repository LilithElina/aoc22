# initialise empty dictionaries
elves = {}
sums = {}

# load input for the first puzzle (how many calories is each elf carrying)
with open("input_data/input_day1.txt", "r") as infile:
    calories = []
    elf = 1
    for line in infile:
        if line.strip():
            cal = int(line)
            calories.append(cal)
        else:
            elves[elf] = calories
            sums[elf] = sum(calories)
            calories = []
            elf += 1
    elves[elf] = calories
    sums[elf] = sum(calories)

# find elf with the most calories
highest_cal = max(sums.values())
most_cal = [key for key, val in sums.items() if val == highest_cal]
print("Highest number of calories: ", highest_cal)
print("Elf carrying the most calories: ", most_cal)